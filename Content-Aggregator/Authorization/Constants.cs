﻿namespace ContentAggregator.Authorization
{
    public class Constants
    {
        public static readonly string AdministratorRole = "Administrator";
        public static readonly string ContentModeratorRole = "ContentModerator";  
    }
}