﻿using System.Threading.Tasks;
using ContentAggregator.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace ContentAggregator.Authorization
{
    public class ContentModeratorAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, Content>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            OperationAuthorizationRequirement requirement,
            Content resource)
        {
            //TODO
            return Task.CompletedTask;
        }
    }
}