﻿namespace ContentAggregator.Services
{
    public interface IRequestUserProvider
    {
        string GetUserId();
    }
}