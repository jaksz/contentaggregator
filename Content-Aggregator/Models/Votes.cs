﻿namespace ContentAggregator.Models
{
    public class Votes
    {
        public int VotesId { get; set; }
        public int ContentId { get; set; }
        public string UserId { get; set; }
        public int Value { get; set; }
    }
}