﻿namespace ContentAggregator.Models.CommentsViewModels
{
    public class CommentsViewModel
    {
        public Comments Comment { get; set; }

        public Content Content { get; set; }
    }
}