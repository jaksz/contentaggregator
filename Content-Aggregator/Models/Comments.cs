﻿namespace ContentAggregator.Models
{
    public class Comments
    {
        public int CommentsId { get; set; }
        public int ContentId { get; set; }
        public string UserId { get; set; }
        public string Text { get; set; }
        public bool IsRemoved { get; set; }
    }
}