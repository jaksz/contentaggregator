﻿using System.Collections.Generic;

namespace ContentAggregator.Models.ContentViewModels
{
    public class ContentCollectionViewModel
    {
        public List<Content> Approved { get; set; }
        public List<Content> Rejected { get; set; }
        public List<Content> Submitted { get; set; }
    }
}