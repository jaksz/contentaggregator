﻿using System.Collections.Generic;

namespace ContentAggregator.Models.ContentViewModels
{
    public class ContentCommentsViewModel
    {
        public Content Content { get; set; }

        public List<Comments> Comments { get; set; }
    }
}