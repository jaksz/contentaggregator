﻿namespace ContentAggregator.Models
{
    public class Content
    {
        public int ContentId { get; set; }
        public string Title { get; set; }
        public string OwnerId { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public ContentStatus Status { get; set; }
    }

    public enum ContentStatus
    {
        Submitted,
        Approved,
        Rejected
    }
}