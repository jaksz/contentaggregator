﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContentAggregator.Models;
using Microsoft.EntityFrameworkCore;

namespace ContentAggregator.Data.Repositories
{
    public class ContentRepository : IContentRepository
    {
        private readonly ApplicationDbContext _context;

        public ContentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Content>> GetApproved()
        {
            return await _context.Content.Where(x => x.Status == ContentStatus.Approved).ToListAsync();
        }

        public async Task<List<Content>> GetSubmitted()
        {
            return await _context.Content.Where(x => x.Status == ContentStatus.Submitted).ToListAsync();
        }

        public async Task<List<Content>> GetRejected()
        {
            return await _context.Content.Where(x => x.Status == ContentStatus.Rejected).ToListAsync();
        }

        public async Task<Content> GetWithId(int id)
        {
            return await _context.Content.SingleOrDefaultAsync(x => x.ContentId == id);
        }

        public bool Exists(int id)
        {
            return _context.Content.Any(m => m.ContentId == id);
        }

        public void Add(Content element)
        {
            _context.Add(element);
        }

        public void Remove(Content element)
        {
            _context.Remove(element);
        }

        public void Update(Content element)
        {
            _context.Update(element);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}