﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ContentAggregator.Models;

namespace ContentAggregator.Data.Repositories
{
    public interface ICommentsRepository : IRepository<Comments>
    {
        Task<Comments> GetWithId(int commentId);
        Task<List<Comments>> GetWithContentId(int contentId);
    }
}