﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContentAggregator.Models;
using Microsoft.EntityFrameworkCore;

namespace ContentAggregator.Data.Repositories
{
    public class CommentsRepository : ICommentsRepository
    {
        private readonly ApplicationDbContext _context;

        public CommentsRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Add(Comments element)
        {
            _context.Add(element);
        }

        public void Remove(Comments element)
        {
            _context.Remove(element);
        }

        public void Update(Comments element)
        {
            _context.Update(element);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public async Task<Comments> GetWithId(int commentId)
        {
            return await _context.Comments.SingleOrDefaultAsync(x => x.CommentsId == commentId);
        }

        public Task<List<Comments>> GetWithContentId(int contentId)
        {
            return _context.Comments.Where(x => x.ContentId == contentId).ToListAsync();
        }
    }
}