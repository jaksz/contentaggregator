﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ContentAggregator.Models;

namespace ContentAggregator.Data.Repositories
{
    public interface IContentRepository : IRepository<Content>
    {
        Task<List<Content>> GetApproved();
        Task<List<Content>> GetSubmitted();
        Task<List<Content>> GetRejected();
        Task<Content> GetWithId(int id);
        bool Exists(int id);
    }
}