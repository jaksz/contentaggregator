﻿using System.Threading.Tasks;

namespace ContentAggregator.Data.Repositories
{
    public interface IRepository<in T>
    {
        void Add(T element);
        void Remove(T element);
        void Update(T element);
        Task SaveChangesAsync();
    }
}