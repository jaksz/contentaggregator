﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ContentAggregator.Authorization;
using ContentAggregator.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace ContentAggregator.Data
{
    public static class DbInitializer
    {
        public static async Task Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                var adminId = await EnsureUser(serviceProvider, "Password1", "admin@example.com");
                await EnsureRole(serviceProvider, adminId, Constants.AdministratorRole);

                var modId = await EnsureUser(serviceProvider, "Password1", "manager@example.com");
                await EnsureRole(serviceProvider, modId, Constants.ContentModeratorRole);

                var userId = await EnsureUser(serviceProvider, "Password1", "user@example.com");

                SeedDb(context, userId);
            }
        }

        private static async Task<string> EnsureUser(IServiceProvider serviceProvider,
            string testUserPw, string UserName)
        {
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

            var user = await userManager.FindByNameAsync(UserName);
            if (user == null)
            {
                user = new ApplicationUser {UserName = UserName, Email = UserName, EmailConfirmed = true};
                await userManager.CreateAsync(user, testUserPw);
            }

            return user.Id;
        }

        private static async Task<IdentityResult> EnsureRole(IServiceProvider serviceProvider,
            string uid, string role)
        {
            IdentityResult IR = null;
            var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();

            if (!await roleManager.RoleExistsAsync(role))
            {
                IR = await roleManager.CreateAsync(new IdentityRole(role));
            }

            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

            var user = await userManager.FindByIdAsync(uid);

            IR = await userManager.AddToRoleAsync(user, role);

            return IR;
        }

        private static void SeedDb(ApplicationDbContext context, string userId)
        {
            if (context.Content.Any())
            {
                return;
            }

            var c1 = context.Content.Add(new Content
            {
                OwnerId = userId,
                Title = "Wyszukiwarka Google",
                Description = "Najlepsza wyszukiwarka",
                Url = "http://google.com",
                Status = ContentStatus.Approved
            });

            AddComments(context, c1.Entity, userId); //TODO: contentId not working

            var c2 = context.Content.Add(new Content
            {
                OwnerId = userId,
                Title = "Reddit",
                Description = "reddit",
                Url = "http://reddit.com",
                Status = ContentStatus.Approved
            });

            AddComments(context, c2.Entity, userId); //TODO: contentId not working

            context.Content.Add(new Content
            {
                OwnerId = userId,
                Title = "Fajny link",
                Description = "zaakceptuj prosze",
                Url = "http://stackoverflow.com",
                Status = ContentStatus.Submitted
            });

            context.SaveChanges();
        }

        private static void AddComments(ApplicationDbContext context, Content content, string userId)
        {
            if (context.Comments.Any(x => x.ContentId == content.ContentId))
            {
                return;
            }

            context.Comments.Add(new Comments
            {
                ContentId = content.ContentId,
                Text = "TEST COMMENT TEST COMMENT TEST COMMENT TEST COMMENT TEST COMMENT TEST COMMENT",
                IsRemoved = false,
                UserId = userId
            });

            context.Comments.Add(new Comments
            {
                ContentId = content.ContentId,
                Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque laoreet sem ut velit vulputate sagittis.",
                IsRemoved = false,
                UserId = userId
            });

            context.Comments.Add(new Comments
            {
                ContentId = content.ContentId,
                Text = "this comment should be removed",
                IsRemoved = true,
                UserId = userId
            });
        }
    }
}