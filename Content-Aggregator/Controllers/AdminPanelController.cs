﻿using System.Linq;
using System.Threading.Tasks;
using ContentAggregator.Data;
using ContentAggregator.Models;
using ContentAggregator.Models.ContentViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ContentAggregator.Controllers
{
    [Authorize(Roles="Administrator")]
    public class AdminPanelController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AdminPanelController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: AdminPanel/Content
        public async Task<IActionResult> Content()
        {
            var approved = await _context.Content.Where(c => c.Status == ContentStatus.Approved).ToListAsync();
            var rejected = await _context.Content.Where(c => c.Status == ContentStatus.Rejected).ToListAsync();
            var submitted = await _context.Content.Where(c => c.Status == ContentStatus.Submitted).ToListAsync();

            var vm = new ContentCollectionViewModel { Approved = approved, Rejected = rejected, Submitted = submitted };

            return View(vm);
        }

        public async Task<IActionResult> Users()
        {
            var users = await _context.Users.ToListAsync();

            return View(users);
        }
    }
}