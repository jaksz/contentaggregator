﻿using System.Threading.Tasks;
using ContentAggregator.Data.Repositories;
using ContentAggregator.Models;
using ContentAggregator.Models.CommentsViewModels;
using ContentAggregator.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ContentAggregator.Controllers
{
    [Authorize]
    public class CommentController : Controller
    {
        private readonly ICommentsRepository _commentsRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IRequestUserProvider _requestUserProvider;

        public CommentController(ICommentsRepository commentsRepository,
            IContentRepository contentRepository,
            IRequestUserProvider requestUserProvider)
        {
            _commentsRepository = commentsRepository;
            _contentRepository = contentRepository;
            _requestUserProvider = requestUserProvider;
        }

        public async Task<IActionResult> Add(int contentId)
        {           
            var content = await _contentRepository.GetWithId(contentId);
            var vm = new CommentsViewModel {Comment = new Comments(), Content = content};
            return View(vm);
        }

        public async Task<IActionResult> Remove(int commentId)
        {
            var comment = await _commentsRepository.GetWithId(commentId);

            comment.IsRemoved = true;

            _commentsRepository.Update(comment);
            await _commentsRepository.SaveChangesAsync();

            return RedirectToAction("Details", "Content", new { id = comment.ContentId });
        }

        public async Task<IActionResult> BringBack(int commentId)
        {
            var comment = await _commentsRepository.GetWithId(commentId);
            comment.IsRemoved = false;

            _commentsRepository.Update(comment);
            await _commentsRepository.SaveChangesAsync();

            return RedirectToAction("Details", "Content", new { id = comment.ContentId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add([Bind("CommentsId,ContentId,IsRemoved,Text,UserId")] Comments comment, int contentId)
        {
            if (!ModelState.IsValid) return View();

            var userId = _requestUserProvider.GetUserId();

            comment.ContentId = contentId;
            comment.UserId = userId;
            comment.IsRemoved = false;

            _commentsRepository.Add(comment);
            await _commentsRepository.SaveChangesAsync();

            return RedirectToAction("Details", "Content", new {id = contentId});
        }
    }
}