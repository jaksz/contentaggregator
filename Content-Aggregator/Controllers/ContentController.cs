﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContentAggregator.Data;
using ContentAggregator.Data.Repositories;
using ContentAggregator.Models;
using ContentAggregator.Models.ContentViewModels;
using ContentAggregator.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Content_Aggregator.Controllers
{
    public class ContentController : Controller
    {
        private readonly IContentRepository _contentRepository;
        private readonly ICommentsRepository _commentsRepository;
        private readonly IRequestUserProvider _requestUserProvider;

        public ContentController(IContentRepository contentRepository,
            ICommentsRepository commentsRepository,
            IRequestUserProvider requestUserProvider)
        {
            _contentRepository = contentRepository;
            _commentsRepository = commentsRepository;
            _requestUserProvider = requestUserProvider;
        }

        // GET: Content
        public async Task<IActionResult> Index()
        {
            return View(await _contentRepository.GetApproved());
        }

        // GET: Content/WaitingRoom
        public async Task<IActionResult> WaitingRoom()
        {
            return View(await _contentRepository.GetSubmitted());
        }

        // GET: Content/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var content = await _contentRepository.GetWithId(id.Value);
            if (content == null)
            {
                return NotFound();
            }

            var comments = await _commentsRepository.GetWithContentId(content.ContentId);
            var vm = new ContentCommentsViewModel
            {
                Content = content,
                Comments = comments
            };

            return View(vm);
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Approve(int contentId)
        {
            var content = await _contentRepository.GetWithId(contentId);
            if (content == null)
            {
                return NotFound();
            }

            content.Status = ContentStatus.Approved;

            _contentRepository.Update(content);
            await _contentRepository.SaveChangesAsync();

            return RedirectToAction("WaitingRoom", "Content");
        }

        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Reject(int contentId)
        {
            var content = await _contentRepository.GetWithId(contentId);
            if (content == null)
            {
                return NotFound();
            }

            content.Status = ContentStatus.Rejected;

            _contentRepository.Update(content);
            await _contentRepository.SaveChangesAsync();

            return RedirectToAction("WaitingRoom", "Content");
        }

        // GET: Content/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }
        // POST: Content/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ContentId,Title,OwnerId,Url,Description,Status")] Content content)
        {
            if (!ModelState.IsValid) return View();

            var ownerId = _requestUserProvider.GetUserId();

            content.OwnerId = ownerId;
            content.Status = ContentStatus.Submitted;

            _contentRepository.Add(content);
            await _contentRepository.SaveChangesAsync();

            return RedirectToAction("Details", "Content", new {id = content.ContentId});
        }

        // GET: Content/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var content = await _contentRepository.GetWithId(id.Value);
            if (content == null)
            {
                return NotFound();
            }
            return View(content);
        }

        // POST: Content/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ContentId,Title,OwnerId,Url,Description,Status")] Content content)
        {
            if (id != content.ContentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _contentRepository.Update(content);
                    await _contentRepository.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ContentExists(content.ContentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(content);
        }

        // GET: Content/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var content = await _contentRepository.GetWithId(id.Value);
            if (content == null)
            {
                return NotFound();
            }

            return View(content);
        }

        // POST: Content/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var content = await _contentRepository.GetWithId(id);

            _contentRepository.Remove(content);
            await _contentRepository.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ContentExists(int id)
        {
            return _contentRepository.Exists(id);
        }
    }
}
