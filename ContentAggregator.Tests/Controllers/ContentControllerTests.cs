﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContentAggregator.Data.Repositories;
using ContentAggregator.Models;
using ContentAggregator.Services;
using Content_Aggregator.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace ContentAggregator.Tests.Controllers
{
    public class ContentControllerTests
    {
        private Mock<IContentRepository> contentRepoMock;
        private Mock<ICommentsRepository> commentsRepoMock;
        private Mock<IRequestUserProvider> requestUserProviderMock;
        private ContentController controller;

        public ContentControllerTests()
        {
            contentRepoMock = new Mock<IContentRepository>();
            commentsRepoMock = new Mock<ICommentsRepository>();
            requestUserProviderMock = new Mock<IRequestUserProvider>();
            controller = new ContentController(
                contentRepoMock.Object, 
                commentsRepoMock.Object, 
                requestUserProviderMock.Object);
        }

        [Fact]
        public async Task IndexTest_ReturnsViewWithContentList()
        {
            var mockContentList = new List<Content>
            {
                new Content {Title = "mock content 1"},
                new Content {Title = "mock content 2"},
            };
            contentRepoMock
                .Setup(r => r.GetApproved())
                .Returns(Task.FromResult(mockContentList));

            var result = await controller.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Content>>(viewResult.ViewData.Model);
            Assert.Equal(2, model.Count());
        }

        [Fact]
        public async Task WaitingRoomTest_ReturnsViewWithContentList()
        {
            var mockContentList = new List<Content>
            {
                new Content {Title = "mock content 1"},
                new Content {Title = "mock content 2"},
            };
            contentRepoMock
                .Setup(r => r.GetSubmitted())
                .Returns(Task.FromResult(mockContentList));

            var result = await controller.WaitingRoom();

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Content>>(viewResult.ViewData.Model);
            Assert.Equal(2, model.Count());
        }

        [Fact]
        public async Task DetailsTest_ReturnsNotFound_WhenNoIdProvided()
        {
            var result = await controller.Details(null);

            Assert.IsType<NotFoundResult>(result);
        }
    }
}